﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class loadForm : Form
    {
        tool.toolGetLog tool = new tool.toolGetLog();
        public loadForm()
        {
            InitializeComponent();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void loadForm_Load(object sender, EventArgs e)
        {
        }
        public static void ValueLoad()
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            label1.Text = "Pls wait";
            tool.toolGetLog tool = new tool.toolGetLog();
            tool.LoadData(folderBrowserDialog1.SelectedPath);
            MessageBox.Show("done");
            label1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmCancel frmCancel = new frmCancel();
            frmCancel.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmTrain frmTrain = new frmTrain();
            frmTrain.Show();
        }

        private void btnTruncate_Click(object sender, EventArgs e)
        {
            tool.TruncateDB();
        }
    }
}
