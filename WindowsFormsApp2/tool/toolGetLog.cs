﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using WindowsFormsApp2.model;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp2.tool
{
    internal class toolGetLog
    {
        logReceipt log = new logReceipt();
        //import file 
        //Save data
        //spilit string
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
        public void LoadData(string path)
        {
            if (File.Exists(path))
            {
                System.Windows.Forms.MessageBox.Show("Test");
            }
            if (path == "")
            {
                System.Windows.Forms.MessageBox.Show("Không có link path");
                return;
            }
            string[] fileArray = Directory.GetFiles(path, "*.log", SearchOption.AllDirectories);
            foreach (string file in fileArray) {
                logReceipt logReceipt = new logReceipt();
                logReceipt = tool1(file);
                if (logReceipt!=null)
                {
                    if (logReceipt.stringCode != null && logReceipt.TransactionEnd != null)
                    {
                        switch (logReceipt.stringCode.Substring(0,5))
                        {
                            case "AVBel":
                                SaveToDB_Cancel(logReceipt);
                                break;

                            case "AVTra":
                                SaveToDB_Trainning(logReceipt);
                                break;

                            case "Beleg":
                                SaveToDB(logReceipt);
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }
        //Create row
        public logReceipt tool1(string Path)
        {
            logReceipt log = new logReceipt();
            if (Path != null)
            {
                if (Path.Contains("Tra_No-"))
                {
                    int index1 = Path.IndexOf("Tra_No-");
                    string stringName = Path.Substring(index1 + 7, 5);
                    if (stringName.Length > 0)
                    {
                        log.TransactionEnd = stringName;
                    }

                }
                string file = File.ReadAllText(Path);
                if (file.Contains("Beleg^"))
                {
                    int index = file.IndexOf("Beleg^");
                    string string2 = file.Substring(index, 50);
                    int index1 = string2.LastIndexOf("^");
                    int index2 = string2.IndexOf(":");
                    if (string2.Length > 0)
                    {
                        
                        log.stringCode = string2;
                        if (index2 - index1 > 0)
                        {
                            log.Price = string2.Substring(index1 + 1, index2 - (index1 + 1)).Replace(" ", "");
                        }
                    }
                }

                if (file.Contains("AVBelegabbruch"))
                {
                    int index = file.IndexOf("AVBelegabbruch");
                    string string2 = file.Substring(index, 50);
                    if (string2.Length > 0)
                    {
                        log.stringCode = string2;
                    }
                }
                //Trainning
                if (file.Contains("AVTraining"))
                {
                    int index = file.IndexOf("AVTraining");
                    string string2 = file.Substring(index, 50);
                    if (string2.Length > 0)
                    {
                        log.stringCode = string2;
                    }
                }
                return log;
            }
            return null;
        }

        //save to db
        public void SaveToDB(logReceipt log)
        {
            if (log.stringCode == null) return;
            try
            {

                MySqlConnection conn = new MySqlConnection(connectionString);
                string queryInsert = string.Format("INSERT INTO `Log`(filename,log,price) VALUE('{0}','{1}','{2}')", log.TransactionEnd, log.stringCode,log.Price);
                MySqlCommand command = new MySqlCommand(queryInsert, conn);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SaveToDB_Cancel(logReceipt log)
        {
            if (log.stringCode == null) return;
            try
            {
                MySqlConnection conn = new MySqlConnection(connectionString);
                string queryInsert = string.Format("INSERT INTO `cancel`(filename,log) VALUE('{0}','{1}')", log.TransactionEnd, log.stringCode);
                MySqlCommand command = new MySqlCommand(queryInsert, conn);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void SaveToDB_Trainning(logReceipt log)
        {
            if (log.stringCode == null) return;
            try
            {
                MySqlConnection conn = new MySqlConnection(connectionString);
                string queryInsert = string.Format("INSERT INTO `train`(filename,log) VALUE('{0}','{1}')", log.TransactionEnd, log.stringCode);
                MySqlCommand command = new MySqlCommand(queryInsert, conn);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public void TruncateDB()
        {
            string queryTruncate1 = "TRUNCATE `log`";
            string queryTruncate2 = "TRUNCATE `Cancel`";
            string queryTruncate3 = "TRUNCATE `Train`";
            MySqlConnection conn = new MySqlConnection(connectionString);
            MySqlCommand command1 = new MySqlCommand(queryTruncate1, conn);
            MySqlCommand command2 = new MySqlCommand(queryTruncate2, conn);
            MySqlCommand command3 = new MySqlCommand(queryTruncate3, conn);
            conn.Open();
            command1.ExecuteNonQuery();
            command2.ExecuteNonQuery();
            command3.ExecuteNonQuery();
            conn.Close();
        }
    }
}
