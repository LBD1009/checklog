﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp2.model;
using System.Configuration;

namespace WindowsFormsApp2.tool
{
    public class dataLoad
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["connection"].ConnectionString;

        public DataTable loadData()
        {
            DataTable dt = new DataTable ();
            MySqlConnection conn = new MySqlConnection(connectionString);
            conn.Open();
            string queryInsert = string.Format("SELECT savelogdata.`log`.`fileName`, savelogdata.`log`.`log`, savelogdata.`log`.`price`,( {0}.`receipt`.`totalPrice` +  {0}.`receipt`.`Tip`) as price " +
                "FROM `log`,`{0}`.Receipt " +
                "WHERE savelogdata.`log`.`fileName` = `{0}`.`receipt`.`TransactionEnd`", ConfigurationManager.AppSettings["DBName"]);

            MySqlDataAdapter command = new MySqlDataAdapter(queryInsert, conn);
            command.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable loadDataTrain()
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(connectionString);
            conn.Open();
            string queryInsert = "SELECT * FROM Train";
            MySqlDataAdapter command = new MySqlDataAdapter(queryInsert, conn);
            command.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable loadDataCancel()
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(connectionString);
            conn.Open();
            string queryInsert = "SELECT * FROM `cancel`";

            MySqlDataAdapter command = new MySqlDataAdapter(queryInsert, conn);
            command.Fill(dt);
            conn.Close();

            return dt;
        }
    }
}


