﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using WindowsFormsApp2.tool;

namespace WindowsFormsApp2.UI
{
    public partial class frmFheckAutoIncr : Form
    {
        CheckIncement checkincrement = new CheckIncement();
        public frmFheckAutoIncr()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 15;
            List<string> lst = checkincrement.checkIncrement(checkincrement.CheckAutoIncrement(txtHost.Text, txtUserID.Text, txtPassword.Text, txtDBName.Text, txtTable.Text));
            progressBar1.Value = 100;
            listBox1.DataSource = lst;
        }
    }
}
