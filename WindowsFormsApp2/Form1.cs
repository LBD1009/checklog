﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2.tool;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        dataLoad dataLoad = new dataLoad();
        tool.toolGetLog tool = new tool.toolGetLog();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dataLoad.loadData();
        }
    }
}
