﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2.tool;

namespace WindowsFormsApp2
{
    public partial class frmTrain : Form
    {
        dataLoad dataLoad = new dataLoad();
        public frmTrain()
        {
            InitializeComponent();
        }

        private void frmTrain_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dataLoad.loadDataTrain();
        }
    }
}
