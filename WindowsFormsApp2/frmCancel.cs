﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2.tool;

namespace WindowsFormsApp2
{
    public partial class frmCancel : Form
    {
        dataLoad dataload = new dataLoad();
        public frmCancel()
        {
            InitializeComponent();
        }

        private void frmCancel_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dataload.loadDataCancel();
        }
    }
}
